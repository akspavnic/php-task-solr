<?php
namespace App\Service;

class Solr
{
    private static $instance;

    /**
     * Solr constructor.
     * @param array $config
     * @throws \SolrIllegalArgumentException
     */
    private function __construct()
    {
        $config = (include __DIR__ . '/../config.php')['solr'];
        self::$instance = new \SolrClient([
            'hostname' => $config['host'],
            'login'    => $config['login'],
            'password' => $config['password'],
            'port'     => $config['port'],
            'path'     => $config['path'],
            'wt'       => $config['wt'],
        ]);
    }

    private function __clone () {}
    private function __wakeup () {}

    /**
     * @return Solr|\SolrClient
     * @throws \SolrIllegalArgumentException
     */
    public static function getInstance()
    {
        if (null !== self::$instance) {
            return self::$instance;
        }

        new self;

        return self::$instance;
    }
}