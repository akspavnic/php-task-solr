<?php
namespace App\Service;


class SolrDocuments
{
    protected $client;

    public function __construct()
    {
        $this->client = Solr::getInstance();
    }

    /**
     * @param array $documents
     * @return \SolrUpdateResponse
     * @throws \SolrClientException
     * @throws \SolrException
     * @throws \SolrIllegalArgumentException
     * @throws \SolrServerException
     */
    public function update(array $documents = []): \SolrUpdateResponse
    {
        $docs = [];
        foreach ($documents as $document) {
            $docs[] = $this->getDocument($document);
        }
        return $this->client->addDocuments($docs, true, 10);
    }

    public function getDocument(array $document): \SolrInputDocument
    {
        $doc = new \SolrInputDocument();

        foreach ($document as $field => $value) {
            $doc->addField($field, $value);
        }

        return $doc;
    }
}