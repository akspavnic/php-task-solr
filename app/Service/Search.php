<?php

namespace App\Service;


class Search
{
    protected $client;
    protected $query;
    protected $search_fields;
    protected $fields = [];
    protected $search_phrase;
    protected $limit = 10;
    protected $offset = 0;
    protected $exact = false;

    protected const SEARCH_FIELDS = [
        'actors',
        'title',
        'tags',
        'categories',
        'description'
    ];

    /**
     * Search constructor.
     * @throws \SolrIllegalArgumentException
     */
    public function __construct()
    {
        $this->client = Solr::getInstance();
    }

    public function getSolrQuery(): \SolrQuery
    {
        if (null === $this->query) {
            $this->query = new \SolrQuery();
        }
        return $this->query;
    }

    public function setSearchFields(array $fields = []): self
    {
        if (!empty($fields)) {
            foreach ($fields as $field) {
                if (is_string($field)) {
                    $this->search_fields[] = \SolrUtils::queryPhrase($field);
                }
            }
        }
        return $this;
    }

    public function setField(string $field): self
    {
        $this->fields[] = $field;

        return $this;
    }

    public function setFields(array $fields = []): self
    {
        if (!empty($fields)) {
            $this->fields = $fields;
        }

        return $this;
    }

    public function setSearchPhrase(string $phrase): self
    {
        $this->search_phrase = \SolrUtils::escapeQueryChars($phrase);
        return $this;
    }

    public function setLimit(int $limit): self
    {
        $this->limit = $limit;
        return $this;
    }

    public function setOffset(int $offset): self
    {
        $this->offset = $offset;
        return $this;
    }

    public function setExact(bool $exact): self
    {
        $this->exact = $exact;
        return $this;
    }

    /**
     * @return array
     * @throws \SolrClientException
     * @throws \SolrServerException
     */
    public function run(): array
    {
        $query = $this->getSolrQuery();
        $q = $this->getQuery();
        $query->setQuery($q)
            ->setStart($this->offset)
            ->setRows($this->limit);
        if (empty($this->fields)) {
            $this->fields = self::SEARCH_FIELDS;
        }
        foreach ($this->fields as $field) {
            $query->addField($field);
        }

        $response = $this->client->query($query);
        $response = $response->getResponse();

        return (array) ($response['response'] ?? []);
    }

    public function getQuery(): string
    {
        if (null === $this->search_fields || empty($this->search_fields)) {
            $this->search_fields = self::SEARCH_FIELDS;
        }
        $query = [];
        foreach ($this->search_fields as $field) {
            if (!$this->exact) {
                $query[] = '(' . $field . ':' . $this->search_phrase . ')';
            } else {
                $query[] = '(' . $field . ':"' . $this->search_phrase . '")';
            }

        }

        return implode(' OR ', $query);
    }


}