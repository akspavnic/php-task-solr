<?php

namespace App\Service;

use PDO;

class Video
{
    protected $db;

    public function __construct()
    {
        $this->db = DB::getInstance();
    }

    public function findFirstById(int $id): array
    {
        $sql = '
SELECT
    v.id,
    v.description,
    v.title,
    GROUP_CONCAT(DISTINCT t.title ORDER BY t.title ASC  SEPARATOR \' \') AS tags,
    GROUP_CONCAT(DISTINCT c.title ORDER BY c.title ASC  SEPARATOR \' \') AS categories,
    GROUP_CONCAT(DISTINCT a.title ORDER BY a.title ASC  SEPARATOR \' \') AS actors
FROM
  videos v
JOIN
  video_has_tags ht ON ht.video_id = v.id
join tags t ON t.id = ht.tag_id
JOIN video_has_categories hc ON hc.video_id = v.id
JOIN categories c ON c.id = hc.category_id
JOIN video_has_actors ha ON ha.video_id = v.id
JOIN actors a ON a.id = ha.actor_id
WHERE v.id = ?
GROUP BY v.id';

        $sth = $this->db->prepare($sql);
        $sth->execute([
            $id
        ]);
        return $sth->fetch(PDO::FETCH_ASSOC);
    }

    public function find(int $limit = 100, int $offset = 0)
    {
        $sql = '
SELECT
    v.id,
    v.description,
    v.title,
    GROUP_CONCAT(DISTINCT t.title ORDER BY t.title ASC  SEPARATOR \' \') AS tags,
    GROUP_CONCAT(DISTINCT c.title ORDER BY c.title ASC  SEPARATOR \' \') AS categories,
    GROUP_CONCAT(DISTINCT a.title ORDER BY a.title ASC  SEPARATOR \' \') AS actors
FROM
  videos v
JOIN
  video_has_tags ht ON ht.video_id = v.id
join tags t ON t.id = ht.tag_id
JOIN video_has_categories hc ON hc.video_id = v.id
JOIN categories c ON c.id = hc.category_id
JOIN video_has_actors ha ON ha.video_id = v.id
JOIN actors a ON a.id = ha.actor_id
GROUP BY v.id
LIMIT ' . $limit . ' OFFSET ' . $offset;

        $sth = $this->db->prepare($sql);
        $sth->execute();

        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }
}