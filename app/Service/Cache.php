<?php

namespace App\Service;

use Symfony\Component\Cache\Adapter\RedisAdapter;
use Symfony\Component\Cache\Simple\RedisCache;
use Symfony\Component\Cache\Simple\FilesystemCache;
use Symfony\Component\Cache\Adapter\MemcachedAdapter;
use Symfony\Component\Cache\Simple\MemcachedCache;

class Cache
{
    private static $instance;

    /**
     * Cache constructor.
     * @throws \ErrorException
     * @throws \Symfony\Component\Cache\Exception\InvalidArgumentException
     */
    private function __construct()
    {
        $config = (include __DIR__ . '/../config.php');
        $config_cache = $config['cache'];
        $adapter = $config_cache['adapter'];

        $cache = null;

        if ($adapter === 'Redis') {
            $redis = $config['redis'];
            $client = RedisAdapter::createConnection('redis://'.$redis['host'] . ':' . $redis['port']);
            $cache = new RedisCache($client, $config_cache['namespace'], $config_cache['ttl']);
        }

        if ($adapter === 'Filesystem') {
            $cache = new FilesystemCache($config_cache['namespace'], $config_cache['ttl'], $config_cache['dir']);
        }

        if ($adapter === 'Memcached') {
            $memcached = $config['memcached'];
            $client = MemcachedAdapter::createConnection('memcached://'.$memcached['host'] . ':' . $memcached['port']);
            $cache = new MemcachedCache($client, $config_cache['namespace'], $config_cache['ttl']);
        }

        if (null === $cache) {
            throw new \ErrorException('Cache adapert is not defined');
        }

        self::$instance = $cache;
    }

    private function __clone () {}
    private function __wakeup () {}

    /**
     * @return FilesystemCache
     * @throws \Symfony\Component\Cache\Exception\InvalidArgumentException
     */
    public static function getInstance()
    {
        if (null !== self::$instance) {
            return self::$instance;
        }

        new self;

        return self::$instance;
    }
}