<?php

namespace App\Service;

use PDO;

class DB
{

    private static $instance;

    private function __construct()
    {
        $config = (include __DIR__ . '/../config.php')['db'];
        self::$instance = new PDO("mysql:dbname={$config['dbanme']};host={$config['host']};port={$config['port']}}", $config['user'], $config['password']);
    }

    private function __clone () {}
    private function __wakeup () {}

    public static function getInstance()
    {
        if (null !== self::$instance) {
            return self::$instance;
        }

        new self;

        return self::$instance;
    }
}


