<?php 
class DbTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    
    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testInstance()
    {
        $db = \App\Service\DB::getInstance();
        $db2 = \App\Service\DB::getInstance();

        $this->assertEquals($db, $db2);
    }
}