<?php 
class SolrDocumentsTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected $documents;
    
    protected function _before()
    {
        $this->documents = new \App\Service\SolrDocuments();
    }

    protected function _after()
    {
    }

    // tests
    public function testGetDocument()
    {
        $doc = $this->documents->getDocument([
            'foo' => 'bar'
        ]);
        $this->assertInstanceOf(\SolrInputDocument::class, $doc);
    }

    public function testUpdate()
    {
        $video = new \App\Service\Video();
        $document = $video->findFirstById(1);
        $response = $this->documents->update([$document]);

        $this->assertInstanceOf(\SolrResponse::class, $response);
    }
}