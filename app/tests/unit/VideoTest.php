<?php 
class VideoTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    private $video;
    
    protected function _before()
    {
        $this->video = new \App\Service\Video();
    }

    protected function _after()
    {
    }

    // tests
    public function testFindFirstById()
    {
        $array = $this->video->findFirstById(1);
        $this->assertArrayHasKey('id', $array);
    }

    public function testFind()
    {
        $array = $this->video->find(1,0);
        $this->assertArrayHasKey('id', $array[0]);
    }
}