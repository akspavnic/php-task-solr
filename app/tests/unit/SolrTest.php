<?php 
class SolrTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    
    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testInstance()
    {
        $solr = \App\Service\Solr::getInstance();
        $solr2 = \App\Service\Solr::getInstance();

        $this->assertEquals($solr, $solr2);
    }
}