<?php 
class CacheTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    
    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testInstance()
    {
        $cache = \App\Service\Cache::getInstance();
        $cache2 = \App\Service\Cache::getInstance();

        $this->assertEquals($cache, $cache2);
    }
}