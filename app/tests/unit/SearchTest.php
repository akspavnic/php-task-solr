<?php 
class SearchTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected $search;
    
    protected function _before()
    {
        $this->search = new App\Service\Search();
    }

    protected function _after()
    {
    }

    // tests
    public function testGetSolrQuery()
    {
        $solrQuery = $this->search->getSolrQuery();
        $this->assertInstanceOf(\SolrQuery::class, $solrQuery);
        $solrQuery = $this->search->getSolrQuery();
        $this->assertInstanceOf(\SolrQuery::class, $solrQuery);
    }

    public function testSetSearchFields()
    {
        $search = $this->search->setSearchFields(['foo']);
        $this->assertInstanceOf(\App\Service\Search::class, $search);
    }

    public function testSetField()
    {
        $search = $this->search->setField('foo');
        $this->assertInstanceOf(\App\Service\Search::class, $search);
    }

    public function testSetFields()
    {
        $search = $this->search->setFields(['foo']);
        $this->assertInstanceOf(\App\Service\Search::class, $search);
    }

    public function testSetSearchPhrase()
    {
        $search = $this->search->setSearchPhrase('Sebastian Bergmann');
        $this->assertInstanceOf(\App\Service\Search::class, $search);
    }

    public function testSetLimit()
    {
        $search = $this->search->setLimit(1);
        $this->assertInstanceOf(\App\Service\Search::class, $search);
    }

    public function testSetOffset()
    {
        $search = $this->search->setOffset(0);
        $this->assertInstanceOf(\App\Service\Search::class, $search);
    }

    public function testSetExact()
    {
        $search = $this->search->setExact(true);
        $this->assertInstanceOf(\App\Service\Search::class, $search);
    }

    public function testGetQuery()
    {
        $q = 'Sebastian';
        $query = [];
        foreach ([
                     'actors',
                     'title',
                     'tags',
                     'categories',
                     'description'
                 ] as $field) {
            $query[] = '(' . $field . ':' . $q . ')';
        }
        $query = implode(' OR ', $query);
        $query2 = $this->search->setSearchPhrase($q)
            ->getQuery();
        $this->assertEquals($query, $query2);
    }

    public function testRun()
    {
        $phrase = 'anglina joly';
        $expected = '{"numFound":1,"start":0,"docs":[{"description":"Wonderland, though she looked down into its mouth open, gazing up into the Dormouse\'s place, and Alice heard the King repeated angrily, \'or I\'ll have you executed.\' The miserable Hatter dropped his.","title":"nobis odit ducimus molestiae quia","tags":"autem quaerat et unde cumque vero quasi laudantium necessitatibus maiores dolores corporis eaque omnis quibusdam saepe explicabo voluptatibus accusantium vero placeat sed sit et eum quia sint sequi veniam inventore fuga eius eius laudantium rem ut quidem","categories":"ab aut rem facere odit et est at saepe consectetur enim nihil vero atque itaque reprehenderit non molestiae similique corrupti saepe dolores porro quia totam occaecati debitis dolore dolor velit fugiat molestiae eligendi facilis odit debitis ut eum sapiente sunt qui molestias perferendis temporibus consequuntur enim est magni","actors":"anglina joly magni dolore autem consequuntur quisquam facere qui consequuntur unde optio aut et cupiditate aut aut optio dolor accusamus vel quo quisquam nihil alias tenetur voluptas nostrum recusandae autem sed maxime voluptatem rem natus voluptatum inventore provident"}]}';
        $response = $this->search->setSearchPhrase($phrase)
            ->setLimit(1)
            ->setOffset(0)
            ->setFields([
                'id1',
                'tags',
                'categories',
                'title',
                'description',
                'actors',
            ])
            ->setExact(false)
            ->run();
        $actual = json_encode($response);

        $this->assertEquals($expected, $actual);
    }


}