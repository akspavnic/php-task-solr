<?php
namespace App\Command;

use App\Service\Solr;
use App\Service\SolrDocuments;
use App\Service\Video;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class IndexerAllCommand extends Command
{
    protected static $defaultName = 'indexer:all';

    /**
     *
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     */
    protected function configure(): void
    {
        $this->setDescription('Indexer all documents');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     * @throws \SolrClientException
     * @throws \SolrException
     * @throws \SolrIllegalArgumentException
     * @throws \SolrServerException
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $output->writeln([
            'Start update index all documents',
        ]);

        $limit = 100;
        $offset = 0;

        $video = new Video();
        while (true) {
            $documents = $video->find($limit, $offset);
            if (empty($documents)) {
                break;
            }

            $solr = new SolrDocuments();
            $solr->update($documents);

            $output->writeln([
                'updated docs: '.($offset+$limit),
            ]);

            $offset += $limit;
        }

        $output->writeln([
            'finish',
        ]);
    }
}