<?php
namespace App\Command;

use App\Service\Solr;
use App\Service\SolrDocuments;
use App\Service\Video;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class IndexerDocumentCommand extends Command
{
    protected static $defaultName = 'indexer:doc';

    /**
     *
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     */
    protected function configure(): void
    {
        $this->setDescription('Indexer document by id');

        $this->addArgument('document_id', InputArgument::REQUIRED);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     * @throws \SolrClientException
     * @throws \SolrException
     * @throws \SolrIllegalArgumentException
     * @throws \SolrServerException
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $id = $input->getArgument('document_id');
        $output->writeln([
            'Update index for document: '.$id,
        ]);

        $video = new Video();
        $document = $video->findFirstById($id);

        $solr = new SolrDocuments();
        $resp = $solr->update([$document]);

        $output->write($resp->getHttpStatus() === 200 ? 'OK' : 'FAIL', 1);
    }
}