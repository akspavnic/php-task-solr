<?php
namespace App\Command;

use App\Service\Cache;
use App\Service\Search;
use App\Service\Solr;
use App\Service\SolrDocuments;
use App\Service\Video;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class SearchCommand extends Command
{
    protected static $defaultName = 'search:video';

    /**
     *
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     */
    protected function configure(): void
    {
        $this->setDescription('Search video');

        $this->addOption('phrase', null, InputOption::VALUE_REQUIRED, 'Input term or phrase for search video', '*');
        $this->addOption('limit', 'l', InputOption::VALUE_OPTIONAL, 'Number of rows (max. 100)', 10);
        $this->addOption('offset', 'o', InputOption::VALUE_OPTIONAL, 'Offset of rows', 0);
        $this->addOption('fields', null, InputOption::VALUE_OPTIONAL,
            'Fields in response',
            'id,tags,categories,title,description,actors');
        $this->addOption('ttl', 'c', InputOption::VALUE_OPTIONAL, 'Cache ttl (seconds, -1 - turn off)', 10);
        $this->addOption('pretty', 'p', InputOption::VALUE_OPTIONAL, 'Pretty output', -1);
        $this->addOption('exact', 'e', InputOption::VALUE_OPTIONAL, 'Exact mode', 0);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws \Symfony\Component\Cache\Exception\InvalidArgumentException
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     * @throws \SolrClientException
     * @throws \SolrException
     * @throws \SolrIllegalArgumentException
     * @throws \SolrServerException
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $time_start = microtime(true);

        $p = $input->getOption('pretty');
        $e = $input->getOption('exact');
        $phrase = $input->getOption('phrase');
        $limit = $input->getOption('limit');
        $offset = $input->getOption('offset');
        $fields = $input->getOption('fields');
        $fields = explode(',', $fields);

        $response = null;

        $ttl = (int)$input->getOption('ttl');
        $is_cache = ($ttl > -1);
        $use_cache = 0;
        if ($is_cache) {
            $cache_key = md5($phrase.$limit.$offset);
            $cache = Cache::getInstance();
            if ($cache->has($cache_key)) {
                $response = $cache->get($cache_key);
                $use_cache = 1;
            }
        }

        if (null === $response) {
            $search = new Search();
            $response = $search->setSearchPhrase($phrase)
                ->setLimit($limit)
                ->setOffset($offset)
                ->setFields($fields)
                ->setExact($e === null)
                ->run();
            if ($is_cache) {
                $cache->set($cache_key, $response, $ttl);
            }
        }

        $result = json_encode($response, ($p === null ? JSON_PRETTY_PRINT : 0));

        $mem_usage = $this->convert(memory_get_usage(true));
        $tet = (microtime(true) - $time_start);

        $output->writeln([
            $result,
            '==========',
            'From cache: '. $use_cache,
            'Mem usage: ' . $mem_usage,
            'Total execution time: ' . $tet . 'mcs',
        ]);
    }

    private function convert($size)
    {
        $unit=array('b','kb','mb','gb','tb','pb');
        return @round($size/pow(1024,($i=floor(log($size,1024)))),2).$unit[$i];
    }
}