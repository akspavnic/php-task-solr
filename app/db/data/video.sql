SELECT
    v.id,
    v.description,
    v.title,
	  GROUP_CONCAT(DISTINCT t.title  order by t.title ASC  SEPARATOR ' ') as tags,
    GROUP_CONCAT(DISTINCT c.title  order by c.title ASC  SEPARATOR ' ') as categories,
    GROUP_CONCAT(DISTINCT a.title  order by a.title ASC  SEPARATOR ' ') as actors
    INTO OUTFILE '/var/www/data/video.csv'
      FIELDS TERMINATED BY ','
      OPTIONALLY ENCLOSED BY '"'
      LINES TERMINATED BY '\n'

FROM videos v
JOIN video_has_tags ht on ht.video_id = v.id
join tags t on t.id = ht.tag_id
JOIN video_has_categories hc on hc.video_id = v.id
JOIN categories c on c.id = hc.category_id
JOIN video_has_actors ha on ha.video_id = v.id
JOIN actors a on a.id = ha.actor_id
GROUP BY v.id

