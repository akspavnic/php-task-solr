<?php
return [
    'db' => [
        'user' => 'root',
        'password' => 'root',
        'host' => 'db',
        'port' => 3306,
        'dbanme' => 'test'
    ],
    'solr' => [
        'host' => 'solr',
        'port' => 8983,
        'login' => '',
        'password' => '',
        'path' => '/solr/mycore',
        'wt' => 'json'
    ],
    'redis' => [
        'host' => 'redis',
        'port' => 6379,
    ],
    'memcached' => [
        'host' => 'memcached',
        'port' => 11211,
    ],
    'cache' => [
        'namespace' => '',
        'ttl' => 180,
        'dir' => __DIR__ . '/cache',
//        'adapter' => 'Redis',
//        'adapter' => 'Memcached',
        'adapter' => 'Filesystem',
    ]
];