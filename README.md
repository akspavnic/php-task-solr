```
    docker-compose up -d --build
```

```
    docker-compose exec db sh -c 'mysql -u root -proot < /var/www/data/create-scheme.sql'
    docker-compose exec db sh -c 'mysql -u root -proot test < /var/www/data/video_dummy_data.sql'
    docker-compose exec db sh -c 'mysql -u root -proot test < /var/www/data/video.sql'    
```

```
    cd app/db/data/ && sh import.sh && cd -
```

```
    docker-compose exec php composer install 
```

```
    docker-compose exec php php vendor/bin/codecept run unit
```

### Commands

```
   docker-compose exec php php bin/cmd list 
```

### Reindex all docs

```
   docker-compose exec php php bin/cmd indexer:all
```

### Reindex doc by id

```
   docker-compose exec php php bin/cmd indexer:doc 1
```

### Search

```
    docker-compose exec php php bin/cmd search:video -h
```

#### anglina

```
    docker-compose exec php php bin/cmd search:video --phrase="anglina joly"
    docker-compose exec php php bin/cmd search:video --phrase="angelina jolie" -c-1 -p #synonym
    docker-compose exec php php bin/cmd search:video --phrase="angelina jolie" -c-1 -p --fields="id,actors"
```

#### other

```
    docker-compose exec php php bin/cmd search:video --phrase="cupiditate aut aut optio" -c-1
    docker-compose exec php php bin/cmd search:video --phrase="cupiditate aut aut optio" -l1
    docker-compose exec php php bin/cmd search:video --phrase="cupiditate aut aut optio" -l1 -o1
```


#### Actor update, reindex, search

```
    docker-compose exec db sh -c 'echo "update actors set title = \"angelina alice\" where id = 201" | mysql -u root -proot test'
    docker-compose exec php php bin/cmd indexer:doc 1
    docker-compose exec php php bin/cmd search:video --phrase="anglina joly" -c-1 -p -e #not found
    docker-compose exec php php bin/cmd search:video --phrase="angelina alice" -c-1 -p -e
    docker-compose exec php php bin/cmd search:video --phrase="hic atque laudantium" -c-1 -p -e
```